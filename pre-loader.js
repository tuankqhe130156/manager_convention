var ButtonLoader = function () {
    var _this = this; 
    // to get uppy companions working, please refer to the official documentation here: https://uppy.io/docs/companion/
    const light_classes = 'spinner spinner-white spinner-right';
    const dark_classes = 'spinner spinner-dark spinner-right';
    // Private functions
    var element;
    function ButtonLoader(element) {
        this.element=element
      }
    var init = function (el) {
        el="#"+el
        _this.element=el
    }
    function start(){
        _this.element.addClass(_this.light_classes)
    }
    function stop(){
        _this.element.removeCLass(_this.light_classes)
    }
    return {
        // public functions
        init: function () {
            initUppy5();
            setTimeout(function () {
            }, 2000);
        }
    };
}();

ButtonLoader.ready(function () {
    ButtonLoader.init();
});


class LoaderBtn {
    constructor(el) {
      this.el = el;
    }
    startLoading(){
        $(this.el).addClass("spinner spinner-white spinner-right")
        $(this.el).attr('disabled','')
    }
    stopLoading(){
        $(this.el).removeClass("spinner spinner-white spinner-right")
        $(this.el).removeAttr('disabled')
    }
  }